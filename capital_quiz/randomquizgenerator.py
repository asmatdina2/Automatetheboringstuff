#! python3.6
# randomQuizGenerator.py - Creates quizzes with questions and answers in
# random order, along with the answer key.


import random, os

class Quiz:
    # create a dictionary with the state as key and capital as value
    # then create a list of all the keys in capitals
    def __init__(self):
        self.capitals = {'Alabama': 'Montgomery', 'Alaska': 'Juneau', 'Arizona': 'Phoenix',
           'Arkansas': 'Little Rock', 'California': 'Sacramento', 'Colorado': 'Denver',
           'Connecticut': 'Hartford', 'Delaware': 'Dover', 'Florida': 'Tallahassee',
           'Georgia': 'Atlanta', 'Hawaii': 'Honolulu', 'Idaho': 'Boise', 'Illinois':
           'Springfield', 'Indiana': 'Indianapolis', 'Iowa': 'Des Moines', 'Kansas':
           'Topeka', 'Kentucky': 'Frankfort', 'Louisiana': 'Baton Rouge', 'Maine':
           'Augusta', 'Maryland': 'Annapolis', 'Massachusetts': 'Boston', 'Michigan':
           'Lansing', 'Minnesota': 'Saint Paul', 'Mississippi': 'Jackson', 'Missouri':
           'Jefferson City', 'Montana': 'Helena', 'Nebraska': 'Lincoln', 'Nevada':
           'Carson City', 'New Hampshire': 'Concord', 'New Jersey': 'Trenton',
           'New Mexico': 'Santa Fe', 'New York': 'Albany', 'North Carolina': 'Raleigh',
           'North Dakota': 'Bismarck', 'Ohio': 'Columbus', 'Oklahoma': 'Oklahoma City',
           'Oregon': 'Salem', 'Pennsylvania': 'Harrisburg', 'Rhode Island': 'Providence',
           'South Carolina': 'Columbia', 'South Dakota': 'Pierre', 'Tennessee':
           'Nashville', 'Texas': 'Austin', 'Utah': 'Salt Lake City', 'Vermont':
           'Montpelier', 'Virginia': 'Richmond', 'Washington': 'Olympia',
           'West Virginia': 'Charleston', 'Wisconsin': 'Madison', 'Wyoming': 'Cheyenne'}


        self.statelist = list(self.capitals.keys())


    # create the directory that holds all the quizzes if it does not already exists
    def crtDir(self):
        if os.path.exists('quizzes\\'):
            pass
        else:
            print('creating quizzes folder')
            os.mkdir('quizzes')


    def crtQuiz(self, amount, qeustions): # amount of files

        for self.quizFile in range(amount):
            # Write header a header in every file
            file = open(f'quizzes\\quiz{self.quizFile}.txt', 'w')
            file.write('Name:\nNumber:\nDate:\nClass:\n')
            file.write('-' * 10 + '\n\n')

            # create answerFile for every file
            answerFile = open(f'quizzes\\answers_quiz{self.quizFile}.txt', 'w')

            # first shuffle the self.statelist for every file.
            random.shuffle(self.statelist)

            for self.stateNum in range(qeustions): # do this per file
                while True:
                    #store the answer and choices in variables
                    self.correctAnswer = self.capitals[self.statelist[self.stateNum]]
                    self.incorrectAnswer1 = self.capitals[self.statelist[random.randint(0, len(self.statelist) - 1)]]
                    self.incorrectAnswer2 = self.capitals[self.statelist[random.randint(0, len(self.statelist) - 1)]]
                    self.incorrectAnswer3 = self.capitals[self.statelist[random.randint(0, len(self.statelist) - 1)]]

                    #create list with answer variables
                    self.multiChoice = [
                    self.incorrectAnswer1,
                    self.incorrectAnswer2,
                    self.incorrectAnswer3]

                    # check if correct answer is in one of the other choices
                    while self.correctAnswer in self.multiChoice[0]:
                        self.multiChoice[0] =  self.capitals[self.statelist[random.randint(0, len(self.statelist) - 1)]]
                    while self.correctAnswer in self.multiChoice[1]:
                        self.multiChoice[1] =  self.capitals[self.statelist[random.randint(0, len(self.statelist) - 1)]]
                    while self.correctAnswer in self.multiChoice[2]:
                        self.multiChoice[2] =  self.capitals[self.statelist[random.randint(0, len(self.statelist) - 1)]]

                    # check if there is no duplicate otherwise go back to the start of this while loop
                    multiChoiceSet = set(self.multiChoice)

                    if len(multiChoiceSet) == len(self.multiChoice):
                        break
                    elif len(multiChoiceSet) == len(self.multiChoice):
                        continue

                # append the the correctAnswer
                self.multiChoice.append(self.correctAnswer)

                # shuffle all the answers
                random.shuffle(self.multiChoice)

                # then write every item from the self.statelist in a question in every file
                file.write(f'\nWhat is the capital of: {self.statelist[self.stateNum]}?\n')
                # and write the items from the self.multiChoice list
                file.write(f'\tA. {self.multiChoice[0]}\n\tB. {self.multiChoice[1]}\n' +
                            f'\tC. {self.multiChoice[2]}\n\tD. {self.multiChoice[3]}\n')

                # write the answer of every qeustion in the answerfile
                answerFile.write(f'capital of {self.statelist[self.stateNum]} is: {self.correctAnswer}\n')


quiz1 = Quiz()
quiz1.crtDir()
quiz1.crtQuiz(10, 10)
